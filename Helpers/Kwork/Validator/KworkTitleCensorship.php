<?php

declare(strict_types=1);

namespace Helpers\Kwork\Validator;

use Model\BanWord;

/**
 * Class KworkTitleCensorship
 * @package Helpers\Kwork\GetKworks
 */
class KworkTitleCensorship
{
    /** @var string[]  */
    private $banWords;

    /** @var array */
    private $matches;

    /**
     * KworkTitleCensorship constructor.
     */
    public function __construct()
    {
        $data = BanWord::all();

        foreach ($data as $item) {
            $this->banWords[] = $item->word;
        }
    }

    /**
     * @param string $title
     * @return bool
     */
    public function validate(string $title): bool
    {
        if ($this->getBanWords($title)) {
            return false;
        }

        return true;
    }

    /**
     * @param string $title
     * @return array
     */
    public function getBanWords(string $title): array
    {
        if (isset($this->matches[$title])) {
            return $this->matches[$title];
        }

        foreach ($this->banWords as $banWord) {
            if (stripos($title, $banWord) !== false) {
                $this->matches[$title][] = $banWord;
            }
        }

        return $this->matches[$title] ?? [];
    }
}
