<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Модель запрещенных слов для таблицы bans_words
 *
 * @property int id
 * @property string word
 */
class BanWord extends Model
{
    protected $table = 'bans_words';
}
