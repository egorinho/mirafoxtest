{strip}
	{if $s eq 'cancelled'}
		{$order.date_cancel|date_format:"%B/%e/%Y"}
	{else}
		{$order.stime|date_format:"%B/%e/%Y"}
	{/if}
{/strip}